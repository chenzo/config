#!/bin/bash


while [ 1 ]
do
    battery=$(acpi |  awk -F ',' '{print $2}' | tr -d "%")
    if [ $battery -lt 10 ]
    then
        echo "$battery %" \
            | osd_cat -c white -O 5 -u red -p bottom -A right -l 1 -d 60 -
    else
        sleep 60
    fi
done
