#!/bin/bash

# Activate mouse middle click
# to find out (look for "Middle Emulation Enabled") :
# - xinput
# xinput list-props 12
# xinput set-prop 12 345 1

mouse_device="SYNA30BD:00 06CB:CE08"
pointer_list=($(xinput \
    | grep "SYNA30BD:00 06CB:CE08" \
    | cut -d '=' -f 2 \
    | cut -f 1))
for p in ${pointer_list[@]}
do
    property_number=$(xinput list-props ${p} \
        | grep "libinput Middle Emulation Enabled (" \
        | cut -d "(" -f 2 \
        | cut -d ")" -f 1)

    if [ -n "${property_number}" ]
    then
        xinput set-prop ${p} ${property_number} 1
    fi
done
