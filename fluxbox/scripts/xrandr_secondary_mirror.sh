#!/bin/bash

# echo "1920/1366" | bc -l  --> 1.40556368960468521229
# echo "1080/768" | bc -l   --> 1.40625000000000000000

xrandr --output VGA-0 --auto --primary
xrandr --output LVDS --off
xrandr --output LVDS --auto --same-as VGA-0 --noprimary

#xrandr --output VGA-0 --mode 1920x1080 --panning 1366x768 --scale 1.4x1.4
