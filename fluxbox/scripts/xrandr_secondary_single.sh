#!/bin/bash

path=/home/lucas/.fluxbox/scripts
secondary_display=`$path/xrandr_get_secondary_display.sh`

xrandr --output ${secondary_display} --auto --primary
#xrandr --output VGA-0 --auto --primary
#xrandr --output HDMI-0 --auto --primary
xrandr --output LVDS --off
