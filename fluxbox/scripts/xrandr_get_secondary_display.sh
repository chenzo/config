#!/bin/bash

display=(` xrandr | grep connected | grep -v disconnected | cut -d " " -f 1 | grep -v LVDS` )

echo $display
