#!/bin/bash

#path=/home/lucas/.fluxbox/scripts;
path=".";
source $path/xrandr_get_displays.sh;

# state 0: Only LVDS
# state 1: Only VGA-0
# state 2: Mirror LVDS on VGA-0

available_display=`get_available_displays`
active_display=`get_active_displays`
inactive_display=`get_inactive_displays`

next_state=0;
if [ "$active_display" == "VGA-0" ]
then
    next_state=2;
elif [ "${active_display}" == "LVDS VGA-0" ]
then
    next_state=0;
elif [ "${active_display}" == "LVDS VGA-0" ]
then
    next_state=0;
fi

echo "available: ${available_display} ${#available_display[@]}";
echo "active: ${active_display}";
echo "inactive: ${inactive_display}";

#xrandr --output ${inactive_display} --auto --primary
#xrandr --output ${active_display} --off

    if [ "$inactive_display" == "VGA-0" ]
    then
        xrandr --output ${inactive_display} --auto --primary
        xrandr --output ${active_display} --off
    fi
