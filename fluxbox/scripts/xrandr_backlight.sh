#!/bin/bash

path=/home/lucas/.fluxbox/scripts

if [ $# -eq 1 ]
then
    if [ $1 == "+" -o $1 == "-" ]
    then
        output_name=`$path/xrandr_get_active_display.sh`
        brightness_level=`xrandr --current --verbose \
            | grep "Brightness" \
            | tr -s "[:space:]" " " \
            | cut -d " " -f 3 \
            `;
        echo "Brightness level: $brightness_level";

        new_value=`echo "${brightness_level} ${1} 0.1" | bc -l`;


        is_gt=`echo "$new_value > 1.0" | bc -l`;
        is_lt=`echo "$new_value < 0.1" | bc -l`;
        if [ $is_gt -eq 1 ]
        then
            new_value=1.0;
        elif [ $is_lt -eq 1 ]
        then
            new_value=0.1;
        fi

        xrandr --output $output_name --brightness $new_value;
        echo "xrandr --output $output_name --brightness $new_value";
        echo "new value: $new_value";

        exit 0;
    fi
fi

echo "Usage: ./xrandr.sh +|-";

exit 1;
