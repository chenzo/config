#!/bin/bash

function get_available_displays
{
    displays=` xrandr \
        | grep connected \
        | grep -v disconnected \
        | cut -d " " -f 1`

    echo $displays;
}

function get_active_displays
{
    displays=`xrandr --listactivemonitors | grep "^ " | awk '{print $4}'`;

    echo $displays;
}

function get_inactive_displays
{
    available_displays=`get_available_displays`
    active_displays=`get_active_displays`

    inactive_displays=`echo "${available_displays} ${active_displays}" \
        | tr -s "[:blank:]" "\n" \
        | sort \
        | uniq -u`;

    echo $inactive_displays;
}
