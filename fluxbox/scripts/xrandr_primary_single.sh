#!/bin/bash

path=/home/lucas/.fluxbox/scripts
secondary_display=`$path/xrandr_get_secondary_display.sh`

xrandr --output LVDS --auto --primary
xrandr --output ${secondary_display} --off
#xrandr --output VGA-0 --off
#xrandr --output HDMI-0 --off
