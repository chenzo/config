#!/bin/bash

#gxmessage -center -borderless -sticky -ontop -geometry 40x20 -fg white -bg black aaa

xmessage -center -buttons skip:0,sleep:1 `acpi`;
if [ $? == 0 ]
then
    echo "continue";
elif [ $? == 1 ]
then
    echo "go to sleep";
fi

exit 0;
