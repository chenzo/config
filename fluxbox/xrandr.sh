#!/bin/bash

# Get extra monitor status
xrandr --listactivemonitors | grep DP-1-1
monitor_1_is_active=$?;
xrandr --listactivemonitors | grep DP-1-2
monitor_2_is_active=$?;
xrandr --listactivemonitors | grep HDMI-1
hdmi_1_is_active=$?;

monitor_1_is_present=$(xrandr --prop | grep DP-1-1 | wc -l);
monitor_2_is_present=$(xrandr --prop | grep DP-1-2 | wc -l);
hdmi_1_is_present=$(xrandr --prop | grep HDMI-1 | wc -l);

# Stop all extra monitors
xrandr --output DP-1-1 --off
xrandr --output DP-1-2 --off
xrandr --output HDMI-1 --off

echo "monitor_1_is_present/active: $monitor_1_is_present $monitor_1_is_active"
echo "monitor_2_is_present/active: $monitor_2_is_present $monitor_2_is_active"
echo "hdmi_1 present/active: $hdmi_1_is_present $hdmi_1_is_active"

# Start extra monitor if needed
if [ $monitor_1_is_present -eq 1 -a $monitor_2_is_present -eq 1 ]
then

    if [ $monitor_1_is_active -eq 1 -a $monitor_2_is_active -eq 1 ]
    then
        xrandr --output DP-1-1 --auto --primary
        xrandr --output DP-1-2 --auto --left-of DP-1-1
    fi
elif [ $monitor_1_is_present -eq 1 ]
then
    if [ $monitor_1_is_active -eq 1 ]
    then
        xrandr --output DP-1-1 --mode 1920x1080
    fi
elif [ $monitor_2_is_present -eq 1 ]
then
    if [ $monitor_2_is_active -eq 1 ]
    then
        xrandr --output DP-1-2 --mode 1920x1080
    fi
fi


if [ $hdmi_1_is_present -eq 1 ]
then
    if [ $hdmi_1_is_active -eq 1 ]
    then
        xrandr --output HDMI-1 --mode 1920x1080
        #xrandr --output HDMI-1 --auto
    fi
fi
