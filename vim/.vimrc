syntax on

" Converts tab to spaces.
set expandtab
" Sets the number of spaces for one tab.
set tabstop=4
" Sets the number of spaces for an auto-indentation.
set shiftwidth=4

set cindent
set smartindent
set autoindent
"set cinkeys=0{,0},:,0#,!,!^F

set textwidth=80

set encoding=utf-8

color darkblue

" OpenSCAD files are almsort like C files.
au BufRead,BufNewFile *.scad setfiletype c

" Incremental search with highlight
" set hlsearch
" set incsearch

"""
" EXTRA
"""

set isf+=:
filetype indent plugin on

" Modify words delimiter : adds character recognized as part of a word.
set isk+=@-@,.,-

" :!aspell -x -l fr -c %
" command -buffer RtTickets $r ! cd /home/lucas/software/rt_exploit/modules && ./cmd_rt_exploit.py my
" <c-r><c-w>

" Lets look at a new buffer without saving the current one.
set hidden

" Show my tiket list
command My
            \ $r 
            \ ! cd /home/lucas/software/gitlab/rt_exploit/modules
            \ && ./cmd_rt_exploit.py my
" Show domain tiket list
command Domain
            \ $r 
            \ ! cd /home/lucas/software/gitlab/rt_exploit/modules
            \ && ./cmd_rt_exploit.py domain
" Show metier tiket list
command Metier
            \ $r 
            \ ! cd /home/lucas/software/gitlab/rt_exploit/modules
            \ && ./cmd_rt_exploit.py metier
" Display a ticket
command -nargs=1 -bar Ticket 
            \ enew 
            \ | 0r 
            \ ! cd /home/lucas/software/gitlab/rt_exploit/modules
            \ && ./cmd_rt_exploit.py ticket ticket_id:<args>
nnoremap ,t :Ticket <c-r><c-w><cr>
" Update a ticket
command Comment 
            \ w! /tmp/rt_comment 
            \ | $r 
            \ ! cd /home/lucas/software/gitlab/rt_exploit/modules
            \ && ./cmd_rt_exploit.py work file:/tmp/rt_comment
nnoremap ,w :Comment
" Display ticket in browser (luakit)
command -nargs=1 Browser
            \ ! luakit https://rt.unistra.fr/rt/Ticket/Display.html?id=<args> &
nnoremap ,b :Browser <c-r><c-w><cr>
" Search for categories
command -nargs=1 -bar Categories
            \ enew 
            \ | 0r /home/lucas/software/rt/categ/category_list.txt
            \ | /<args>
nnoremap ,c :Categories <c-r><c-w><cr>
" Steal a ticket
command -nargs=1 Steal
            \ | $r
            \ ! cd /home/lucas/software/gitlab/rt_exploit/modules
            \ && ./cmd_rt_exploit.py steal ticket_id:<args>
nnoremap ,s :Steal <c-r><c-w><cr>
" LDAP search
command -nargs=1 -bar Ldap
            \ $r
            \ ! cd /home/lucas/software/ldap_exploit/modules/
            \ && ./cmd_ldap_search.py <args>
nnoremap ,l :Ldap <c-r><c-w><cr>
