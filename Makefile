all: \
	make_bash \
	make_cmus \
	make_beets \
	make_feh \
	make_fluxbox \
	make_khal \
	make_luakit \
	make_mailcap \
	make_mutt \
	make_vim \
	make_vimperator \
	make_xdg-mime

make_bash:
	@ echo "Configure: bash"
	@ cp bash/.bashrc ~/.bashrc
	@ mkdir -p ~/.bash
	@ cp bash/.bash/* ~/.bash/.

make_beets:
	@ echo "Configure: beets"
	@ mkdir -p ~/.config/beets
	@ cp -r beets/* ~/.config/beets/.
	@ echo "beet import ~/perso/loisirs/music/ogg/"

make_cmus:
	@ echo "Configure: cmus"
	@ mkdir -p ~/.config/cmus
	@ cp -r cmus/* ~/.config/cmus/.
	@ echo ":colorscheme chenzo"

make_feh:
	@ echo "Configure: feh"
	@ mkdir -p ~/.config/feh
	@ cp -r feh/* ~/.config/feh/.

make_fluxbox:
	@ echo "Configure: fluxbox"
	@ mkdir -p ~/.fluxbox
	@ cp -r fluxbox/* ~/.fluxbox/.

make_khal:
	@ echo "Configure: khal"
	@ mkdir -p ~/.config/khal
	@ cp -r khal/* ~/.config/khal/.

make_luakit:
	@ echo "Configure: luakit"
	@ mkdir -p ~/.config/luakit
	@ cp luakit/* ~/.config/luakit/.
	@ curl -s "https://easylist-downloads.adblockplus.org/easylist.txt" -o ~/.local/share/luakit/adblock/easylist.txt
	@ echo "To enable open page: luakit://adblock/"

make_mailcap:
	@ echo "Configure: mailcap"
	@ cp mailcap/.mailcap ~/.mailcap

make_mutt:
	@ echo "Configure: mutt"
	@ cp mutt/.muttrc ~/.muttrc
	@ mkdir -p ~/.mutt/config
	@ cp mutt/.mutt/config/* ~/.mutt/config/.

make_vim:
	@ echo "Configure: vim"
	@ cp vim/.vimrc ~/.vimrc

make_vimperator:
	@ echo "Configure: vimperator"
	@ cp vimperator/.vimperatorrc ~/.vimperatorrc

make_xdg-mime:
	@ echo "[Look at /usr/share/applications/*.desktop for other applications]"
	@ xdg-mime default org.pwmt.zathura.desktop application/pdf
	@ echo "- application/pdf:" && xdg-mime query default application/pdf
	@ xdg-mime default feh.desktop image/png
	@ echo "- image/png:" && xdg-mime query default image/png
