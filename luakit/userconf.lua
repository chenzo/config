-- Default home page
local settings = require "settings"
settings.window.home_page = ""

-- Download location
-- require "downloads"
-- downloads.default_dir = os.getenv("HOME") .. "/Downloads"

-- Search engines
local search_engines = settings.window.search_engines
search_engines.cnrtl = "http://www.cnrtl.fr/definition/%s"
search_engines.conj = "http://leconjugueur.lefigaro.fr/conjugaison/verbe/%s.html"
-- https://conjugator.reverso.net/conjugation-french-verb-%s.html
search_engines.duck = "https://duckduckgo.com/?q=%s&t=debian"
search_engines.google = "https://google.co.uk/search?q=%s"
search_engines.pons = "http://en.pons.com/translate?q=%s&l=enfr&in=&lf=fr"
search_engines.qwant = "https://www.qwant.com/?l=en&h=1&hc=1&a=0&s=0&b=1&i=0&r=GB&sr=en&q=%s"
search_engines.wiki = "https://en.wikipedia.org/wiki/Special:Search?search=%s"
---- Set default search engine
search_engines.default = search_engines.qwant


-- Key bindings
local modes = require "modes"
modes.add_binds("normal", {
-- {"<key>",
--  "<description>",
--  function (w) w:enter_cmd("<command>") end}
    {
        "^,c$",
        "Cnrtl search",
        function (w) w:enter_cmd(":tabopen cnrtl ") end
    },
    {
        "^,d$",
        "DuckDuckGo search",
        function (w) w:enter_cmd(":tabopen duckduckgo ") end
    },
    {
        "^,g$",
        "Google search",
        function (w) w:enter_cmd(":tabopen google ") end
    },
    {
        "^,p$",
        "Pons search",
        function (w) w:enter_cmd(":tabopen pons ") end
    },
    {
        "^,q$",
        "Qwant search",
        function (w) w:enter_cmd(":tabopen qwant ") end
    },
    {
        "^,w$",
        "Wikipedia search",
        function (w) w:enter_cmd(":tabopen wiki ") end
    }
})
