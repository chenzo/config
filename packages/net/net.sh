#!/bin/bash

wlan_if="wlp2s0"

function is_iw_associated
{
#    iwgetid | grep -v '""'
    iw wlp2s0 info | grep ssid
}

function wait_until_iw_associated
{
    is_iw_associated
    while [ $? -eq 1 ]
    do
        echo "iw is not associated yet"
        sleep 1
        is_iw_associated
    done
    echo "iw is associated"
}

echo "Killing dhclient"
killall dhclient &>/dev/null
echo "Killing wpoa_suuplicant"
killall wpa_supplicant &>/dev/null

echo "Starting wpa_supplicant"
wpa_supplicant -i ${wlan_if} -c /root/wpa.conf &>/dev/null &

wait_until_iw_associated

echo "Starting dhclient"
dhclient -v ${wlan_if}
