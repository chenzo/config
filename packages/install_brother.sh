#!/bin/bash

directory=`pwd`


#su - -c "\
#    apt-get install lib32stdc++6 \
#    && dpkg -i --force-all ${directory}/brother/dcp7065dnlpr-2.1.0-1.i386.deb \
#    && dpkg -i --force-all ${directory}/brother/cupswrapperDCP7065DN-2.0.4-2.i386.deb \
#    "

su - -c "\
    dpkg -i ${directory}/brother/brscan4-0.4.4-4.amd64.deb \
    && brsaneconfig4 -a name=DCP7065DN model=DCP7065DN ip=192.168.1.254 \
    "

#su - -c "\
#    systemctl restart cups"

su - -c '\
    sed -i \
    "s|^DeviceURI usb:/dev/usb/lp0$|DeviceURI lpd://192.168.1.254/binary_p1|" \
    /etc/cups/printers.conf'

su - -c "\
    systemctl restart cups"

# lpd://192.168.1.254/binary_p1
