#!/bin/bash

gpg=`curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg`


su -c "echo \"${gpg}\" | apt-key add - \
    && add-apt-repository \
        \"deb [arch=amd64] https://download.docker.com/linux/debian \
        buster \
        stable\""

#su -c "add-apt-repository \
#    \"deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo \"$ID\") \
#    $(lsb_release -cs) \
#    stable\""
