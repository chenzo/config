# NPM
if which npm >/dev/null; then
  source <(npm completion)
fi
# NVM
export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Kubectl
if which kubectl >/dev/null; then
  source <(kubectl completion bash)
fi

# Beet
eval "$(beet completion)"

